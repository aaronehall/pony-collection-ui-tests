﻿using LightBDD.Framework.Scenarios.Basic;
using LightBDD.XUnit2;
using OpenQA.Selenium;
using PonyCollection.Ui.Tests.Infrastructure;
using PonyCollection.Ui.Tests.Infrastructure.PageObjects;
using System;
using System.Linq;
using Xunit;

namespace PonyCollection.Ui.Tests.Bdd
{
    public class ColorsPage_should_ : FeatureFixture, IDisposable
    {
        private IWebDriver _driver;
        private Table _colorsTable;

        public ColorsPage_should_()
        {
            _driver = BrowserFactory.GetDriver();
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        }

        [Scenario]
        public void display_colors_table()
        {
            Runner.RunScenario(
                When_I_navigate_to_the_colors_page,
                Then_the_colors_table_is_visible
            );
        }

        private void When_I_navigate_to_the_colors_page()
        {
            _colorsTable = new PonyCollectionApp(_driver).GoToColorsPage().ColorsTable;
        }

        private void Then_the_colors_table_is_visible()
        {
            Assert.Equal("Name", _colorsTable.ColumnHeaders.ElementAt(0).Text);
        }

        public void Dispose()
        {
            _driver.Dispose();
        }
    }
}
