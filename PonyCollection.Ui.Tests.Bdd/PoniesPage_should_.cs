﻿using LightBDD.Framework;
using LightBDD.Framework.Scenarios.Basic;
using LightBDD.XUnit2;
using OpenQA.Selenium;
using PonyCollection.Ui.Tests.Infrastructure;
using PonyCollection.Ui.Tests.Infrastructure.PageObjects;
using System;
using System.Linq;
using Xunit;

namespace PonyCollection.Ui.Tests.Bdd
{
    [FeatureDescription(@"
        As a Pony Collection user,
        I want to be able to view a page displaying all my ponies
        So I can know which ponies are in my collection
    ")]
    public class PoniesPage_should_
        : FeatureFixture, IDisposable
    {
        private IWebDriver _driver;
        private Table _poniesTable;

        public PoniesPage_should_()
        {
            _driver = BrowserFactory.GetDriver();
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        }

        [Scenario]
        public void display_ponies_table()
        {
            Runner.RunScenario(
                When_I_navigate_to_the_ponies_page,
                Then_the_ponies_table_is_visible
            );
        }

        private void When_I_navigate_to_the_ponies_page()
        {
            _poniesTable = new PonyCollectionApp(_driver).GoToPoniesPage().PoniesTable;
        }

        private void Then_the_ponies_table_is_visible()
        {
            Assert.Equal("Name", _poniesTable.ColumnHeaders.ElementAt(0).Text);
            Assert.Equal("Body Color", _poniesTable.ColumnHeaders.ElementAt(1).Text);
            Assert.Equal("Mane Color", _poniesTable.ColumnHeaders.ElementAt(2).Text);
            Assert.Equal("Pony Type", _poniesTable.ColumnHeaders.ElementAt(3).Text);
        }

        public void Dispose()
        {
            _driver.Dispose();
        }
    }
}
