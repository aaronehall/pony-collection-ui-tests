﻿using LightBDD.Framework.Scenarios.Basic;
using LightBDD.XUnit2;
using OpenQA.Selenium;
using PonyCollection.Ui.Tests.Infrastructure;
using PonyCollection.Ui.Tests.Infrastructure.PageObjects;
using System;
using Xunit;

namespace PonyCollection.Ui.Tests.Bdd
{
    public class PonyCollectionManager_app_should_ : FeatureFixture, IDisposable
    {
        private IWebDriver _driver;
        private string _headerText;

        public PonyCollectionManager_app_should_()
        {
            _driver = BrowserFactory.GetDriver();
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        }

        [Scenario]
        public void display_header()
        {
            Runner.RunScenario(
                when_I_navigate_to_the_pony_collection_manager_app,
                then_the_header_text_displays
            );
        }

        private void when_I_navigate_to_the_pony_collection_manager_app()
        {
            _headerText = new PonyCollectionApp(_driver).HeaderText;
        }

        private void then_the_header_text_displays()
        {
            Assert.Equal("Pony Collection Manager", _headerText);
        }

        public void Dispose()
        {
            _driver.Dispose();
        }
    }
}
