﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using PonyCollection.Ui.Tests.Infrastructure.PageObjects;
using System;

namespace PonyCollection.Ui.Tests.Infrastructure
{
    public class BaseTest : IDisposable
    {
        private PonyCollectionApp _app;
        private IWebDriver _driver;

        public BaseTest()
        {
            _driver = BrowserFactory.GetDriver();
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        }

        protected PonyCollectionApp App
        {
            get
            {
                if (_app == null)
                {
                    _app = new PonyCollectionApp(_driver);
                }

                return _app;
            }
        }

        public void Dispose()
        {
            _driver.Dispose();
            _app = null;
        }
    }
}
