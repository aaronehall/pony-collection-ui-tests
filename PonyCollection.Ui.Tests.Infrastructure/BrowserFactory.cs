﻿using Microsoft.Extensions.Configuration;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;

namespace PonyCollection.Ui.Tests.Infrastructure
{
    public static class BrowserFactory
    {
        public static IWebDriver GetDriver()
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appSettings.json")
                .Build();

            var browserName = config["browser"].ToLower();

            switch (browserName)
            {
                case "firefox":
                    return new FirefoxDriver();
                case "chrome":
                    return new ChromeDriver();
                default:
                    throw new NotSupportedException("Unrecognized browser");
            }
        }
    }
}
