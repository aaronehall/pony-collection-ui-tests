﻿using OpenQA.Selenium;

namespace PonyCollection.Ui.Tests.Infrastructure.PageObjects
{
    public class ColorsPage
    {
        private readonly ISearchContext _searchContext;

        public ColorsPage(ISearchContext searchContext)
        {
            _searchContext = searchContext;
        }

        public Table ColorsTable => new Table(_searchContext.FindElement(By.Id("colorsTable")));
    }
}
