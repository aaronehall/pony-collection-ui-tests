﻿using OpenQA.Selenium;

namespace PonyCollection.Ui.Tests.Infrastructure.PageObjects
{
    public class NavigationLinks
    {
        private readonly ISearchContext _searchContext;

        public NavigationLinks(ISearchContext searchContext)
        {
            _searchContext = searchContext;
        }

        public IWebElement PoniesLink => _searchContext.FindElement(By.Id("poniesLink"));

        public IWebElement ColorsLink => _searchContext.FindElement(By.Id("colorsLink"));

        public IWebElement PonyTypesLink => _searchContext.FindElement(By.Id("ponyTypesLink"));
    }
}
