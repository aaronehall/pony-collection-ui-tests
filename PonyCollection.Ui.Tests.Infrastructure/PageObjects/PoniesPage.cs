﻿using OpenQA.Selenium;

namespace PonyCollection.Ui.Tests.Infrastructure.PageObjects
{
    public class PoniesPage
    {
        private readonly ISearchContext _searchContext;

        public PoniesPage(ISearchContext searchContext)
        {
            _searchContext = searchContext;
        }

        public Table PoniesTable => new Table(_searchContext.FindElement(By.Id("poniesTable")));
    }
}
