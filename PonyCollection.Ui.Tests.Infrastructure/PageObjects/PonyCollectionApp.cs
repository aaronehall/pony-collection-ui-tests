﻿using OpenQA.Selenium;
using System;

namespace PonyCollection.Ui.Tests.Infrastructure.PageObjects
{
    public class PonyCollectionApp
    {
        private readonly IWebDriver _driver;

        public PonyCollectionApp(IWebDriver driver)
        {
            _driver = driver;
            _driver.Manage().Window.Maximize();
            _driver.Navigate().GoToUrl("http://localhost:8080");
        }

        private NavigationLinks NavigationLinks => new NavigationLinks(_driver.FindElement(By.Id("headerLinks")));

        public string HeaderText => _driver.FindElement(By.CssSelector(".page-header h1")).Text;

        public PoniesPage GoToPoniesPage()
        {
            NavigationLinks.PoniesLink.Click();
            return new PoniesPage(_driver.FindElement(By.Id("poniesPage")));
        }

        public ColorsPage GoToColorsPage()
        {
            NavigationLinks.ColorsLink.Click();
            return new ColorsPage(_driver.FindElement(By.Id("colorsPage")));
        }
    }
}
