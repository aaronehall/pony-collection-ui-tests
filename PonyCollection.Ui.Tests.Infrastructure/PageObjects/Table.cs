﻿using OpenQA.Selenium;
using System.Collections.Generic;

namespace PonyCollection.Ui.Tests.Infrastructure.PageObjects
{
    public class Table
    {
        private readonly ISearchContext _searchContext;

        public Table(ISearchContext searchContext)
        {
            _searchContext = searchContext;
        }

        public ICollection<IWebElement> ColumnHeaders => _searchContext.FindElements(By.CssSelector("thead tr th"));
    }
}
