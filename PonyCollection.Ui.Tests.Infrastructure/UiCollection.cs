﻿using Xunit;

namespace PonyCollection.Ui.Tests.Infrastructure
{
    [CollectionDefinition("UI Tests")]
    public class UiCollection : ICollectionFixture<UiTestFixture>
    {
        public UiTestFixture Fixture;

        public UiCollection(UiTestFixture fixture)
        {
            Fixture = fixture;
        }
    }
}
