﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System;

namespace PonyCollection.Ui.Tests.Infrastructure
{
    public class UiTestFixture : IDisposable
    {
        public IWebDriver Driver = new FirefoxDriver();

        public void Dispose()
        {
            Driver.Dispose();
        }
    }
}
