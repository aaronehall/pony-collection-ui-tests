﻿using PonyCollection.Ui.Tests.Infrastructure;
using System.Linq;
using Xunit;

namespace PonyCollection.Ui.Tests
{
    public class ColorsPage_should_ : BaseTest
    {
        [Fact]
        public void display_colors_table()
        {
            var colorsTable = App.GoToColorsPage().ColorsTable;

            Assert.Equal("Name", colorsTable.ColumnHeaders.ElementAt(0).Text);
        }
    }
}
