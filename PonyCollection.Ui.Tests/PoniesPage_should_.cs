﻿using PonyCollection.Ui.Tests.Infrastructure;
using System.Linq;
using Xunit;

namespace PonyCollection.Ui.Tests
{
    public class PoniesPage_should_ : BaseTest
    {
        [Fact]
        public void display_ponies_table()
        {
            var poniesTable = App.GoToPoniesPage().PoniesTable;

            Assert.Equal("Name", poniesTable.ColumnHeaders.ElementAt(0).Text);
            Assert.Equal("Body Color", poniesTable.ColumnHeaders.ElementAt(1).Text);
            Assert.Equal("Mane Color", poniesTable.ColumnHeaders.ElementAt(2).Text);
            Assert.Equal("Pony Type", poniesTable.ColumnHeaders.ElementAt(3).Text);
        }
    }
}
