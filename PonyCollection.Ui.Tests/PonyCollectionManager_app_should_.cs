using PonyCollection.Ui.Tests.Infrastructure;
using Xunit;

namespace PonyCollection.Ui.Tests
{
    public class PonyCollectionManager_app_should_ : BaseTest
    {
        [Fact]
        public void display_header()
        {
            Assert.Equal("Pony Collection Manager", App.HeaderText);
        }
    }
}
